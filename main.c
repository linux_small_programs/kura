#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> // isalnum()
#include <stdbool.h>
#include <time.h> // time()
#define MAX_NAME_LENGTH 30
#define MAX_ELECTION 100

typedef struct{
  bool enumerate;
  bool unique;
  uint number_of_element;
}Options_t;


uint *RANDOM_NUMBERS;

void print_names(char **name_list, uint length){
  int i;
  for( i = 0; i < length; i++){
    printf("%d) %s\n", i + 1, name_list[i]);
  }
}

bool check_random_number(uint *number, uint range, int n){
  int j;
  *number = random() % range;
  for ( j = n - 1 ; j >= 0 ; j--){
    if (*number == RANDOM_NUMBERS[j]) {
      return false;
    }
  }
  return true;
}

void generate_random_numbers(uint range, int n, bool dup_prot){
  int i;
  if (dup_prot){ // Every element picked once
    for( i = 0; i < n ; i++){
      while(!check_random_number(&RANDOM_NUMBERS[i], range, i ));
    }
  }
  else {
    for( i = 0; i < n ; i++ ){
      RANDOM_NUMBERS[i] = random() % range; 
    } 
  }
}

void print_selected_elements(char **name_list, uint range, int n, bool dup_prot, bool enumerate ) {
  generate_random_numbers(range, n, dup_prot);
  int i;
  if(enumerate){
    for( i = 0; i < n; i++ )
      fprintf(stdout, "%d) %s\n", i + 1, name_list[ RANDOM_NUMBERS[i] ]);
  }
  else{
    for( i = 0; i < n; i++ )
      fprintf(stdout, "%s\n", name_list[ RANDOM_NUMBERS[i] ]);
  }
}

int number_of_element = 0;
int name_cap = 100;


int main(int argc,char *argv[]){
  srandom( time(NULL) );

  Options_t opts;
  opts.number_of_element = 1;
  opts.enumerate = false;
  opts.unique = false;
  
  int i;
  for (i = 0 ; i < argc ; i++ ){
    if(strstr(argv[i], "-h")){
      fprintf(stderr,"Usage: cat <your list> | %s <options>\n\
                     -h               Prints this help menu\n\
                     -u               All unique elements\n\
                     -n <number>      Number of element\n\
                     -e               Enumarated output\n", argv[0]);
      return 0;
    }
    if(strstr(argv[i], "-u")){
      opts.unique = true;
    }
    if(strstr(argv[i], "-e")){
      opts.enumerate = true;
    }
    if(strstr(argv[i], "-n")){
      int tmp = atoi(argv[i + 1]);
      if(tmp != 0){
	opts.number_of_element = tmp;
      }
    }
  }

  RANDOM_NUMBERS = malloc(sizeof(uint) * opts.number_of_element);

  // Needed memory allocation
  char **name_list = malloc(sizeof(char *) * name_cap);
  if( name_list == NULL){
    perror("Not Enough Memory!");
    return EXIT_FAILURE;
  }
  char *names = malloc(sizeof(char) * name_cap * MAX_NAME_LENGTH);
  if( names == NULL){
    perror("Not Enough Memory!");
    return EXIT_FAILURE;
  }

  int c;
  char **list_ptr = name_list;
  char *str_ptr = names;
  int list_ctr = 0;
  int mem_ctr = 0;
  list_ptr[list_ctr] = str_ptr;
  list_ctr++;
  while((c = getc(stdin)) != EOF){
    if( name_cap * MAX_NAME_LENGTH - mem_ctr < 2 * MAX_NAME_LENGTH){ // low mem case
      name_cap *= 2;
      char *tmp = realloc(names, name_cap * MAX_NAME_LENGTH);
      if( tmp == NULL){
	perror("Not Enough Memory!");
	return EXIT_FAILURE;
      }
      names = tmp;

      char **tmp2 = realloc(name_list, name_cap);
      if( tmp2== NULL){
	perror("Not Enough Memory!");
	free(names);
	return EXIT_FAILURE;
      }

      name_list = tmp2;

    }
    if( c != '\n') {
      *str_ptr = c;
      str_ptr++;
      mem_ctr++;
    }
    else{
      *str_ptr = '\0';
      str_ptr++;
      list_ptr[list_ctr] = str_ptr;
      list_ctr++;
    }
  }

  if( (opts.number_of_element > list_ctr) && opts.unique){
    fprintf(stderr, "ERROR: Number of wanted elements higher than input elements with unique flag.\n");
    return EXIT_FAILURE;
  }
  print_selected_elements(name_list, list_ctr - 1 , opts.number_of_element, opts.unique, opts.enumerate);

  free(RANDOM_NUMBERS);
  free(name_list);
  free(names);
  return 0;
}
