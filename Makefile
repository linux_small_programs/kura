##
# kura
#

CC = gcc
CFLAGS = -g -Wall
FILES = main.c
OBJ := $(FILES:.c=.o)
OUTPUT = kura


all: run

%.o:%.c
	$(CC) -c $< -o $@ $(CFLAGS)

compile: $(OBJ)
	$(CC) $< -o $(OUTPUT)

run: compile
	clear
	./$(OUTPUT)

clean:
	rm $(OUTPUT)
	rm $(OBJ)


# end
